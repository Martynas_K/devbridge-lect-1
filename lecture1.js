let a, b, c;
/*
    1. Write two binary functions, add and mul, that take two numbers and return their sum and product.

    add(3, 4);    //  7
    mul(3, 4);    // 12
*/
const add = (a, b) => a + b;
const mul = (a, b) => a * b;
console.log('Task 01: ' + add(3, 4));
console.log('Task 01: ' + mul(3, 4));

/*
    2. Write a function that takes an argument and returns a function that returns that argument.

    const idf = identify(3);
    idf();    // 3
*/
const identify = a => a;
const idf = identify(3);
console.log('Task 02: ' + idf);

/*
    3. Write a function that adds from two invocations.

    addf(3)(4);    // 7
*/
const addf = a => b => (a + b);
console.log('Task 03: ' + addf(3)(4));

/*
    4. Write a function that takes a binary function, and makes it callable with two invocations.

    const addf = applyf(add);
    addf(3)(4);           // 7
    applyf(mul)(5)(6);    // 30
*/
const applyf = fn => a => b => fn(a, b);
console.log('Task 04: ' + applyf(mul)(5)(6));

/*
    5. Write a function that takes a function and an argument, and returns a function that can supply a second argument.

    const add3 = curry(add, 3);
    add3(4);             // 7
    curry(mul, 5)(6);    // 30
*/
const curry = (fn, a) => b => fn(a, b);
console.log('Task 05: ' + curry(mul,5)(6));

/*
    6. Write a function twice that takes a binary function and returns a unary function that passes its argument to the binary function twice.

    const double = twice(add);
    double(11);    // 22
    const square = twice(mul);
    square(11);    // 121
*/
const twice = binaryFn => a => binaryFn(a, a);
const double = twice(add);
const square = twice(mul);
console.log('Task 06: ' + double(11));
console.log('Task 06: ' + square(11));

/*
    7. Write a function composeu that takes two unary functions and returns a unary function that calls them both.

    composeu(double, square)(3);    // 36
*/
const composeu = (fn1, fn2) => a => fn2(fn1(a)) ;
console.log('Task 07: ' + composeu(double, square)(3));

/*
    8. Write a function composeb that takes two binary functions and returns a function that calls them both.

    composeb(add, mul)(2, 3, 5);    // 25
*/
const composeb = (fn1, fn2) => (a, b, c) => fn2(fn1(a, b), c);
console.log('Task 08: ' + composeb(add, mul)(2, 3, 5));

/*
    9. Write a factory function that returns two functions that implement an up/down counter.

    const counter = counterf (10);
    counter.inc();    // 11
    counter.dec();    // 10
*/
const counterf = num => ({
    inc: () => ++num,
    dec: () => --num
});
const counter = counterf (10);
console.log('Task 09: ' + counter.inc());
console.log('Task 09: ' + counter.dec());

/*
    10. Make an array wrapper Class with methods get, store, and append, such that no access to the private array is provided.

    const myVector = Vector([]);
    myVector.append(7);
    myVector.store(1, 8);
    myVector.get(0);    // 7
    myVector.get(1);    // 8
*/
class Vector {
    constructor(array) {
        this.array = array;
    }
    get = index => this.array[index];
    store = (index, value) => {
        this.array[index] = value
    };
    append = value => this.array.push(value);
}

const myVector = new Vector([]);
myVector.append(7);
myVector.store(1, 8);
console.log('Task 10: ' + myVector.get(0));
console.log('Task 10: ' + myVector.get(1));

/*
  11. Write a function that adds from many invocations, until it sees an empty invocation.

  addg(3)(4)(5)();     // 12
  addg(1)(2)(4)(8)();  // 15
*/

function addg(value) {
    function nextInvocation(nextValue) {
        if (nextValue === undefined) {
            return value;
        }
        value += nextValue;
        return nextInvocation;
    }
    if (value !== undefined) {
        return nextInvocation;
    }
}
console.log('Task 11: ' + addg(3)(4)(5)());
console.log('Task 11: ' + addg(1)(2)(4)(8)());

/*
  12. Write a function that will take a binary function and apply it to many invocations.

  applyg(add)(3)(4)(5)();       // 12
  applyg(mul)(1)(2)(4)(8)();    // 64
*/

function applyg(binary) {
    return function getValue (value) {
        if (value === undefined) {
            return value;
        }
        return function getNextValue(nextValue) {
            if (nextValue === undefined) {
                return value;
            }
            value =  binary(value, nextValue);
            return getNextValue;
        }
    }
}
console.log('Task 12: ' + applyg(add)(3)(4)(5)());
console.log('Task 12: ' + applyg(mul)(1)(2)(4)(8)());

/*
  13. Make a function that returns a function that will return the next fibonacci number.

  const fib = fibonaccif();
  fib();    // 0
  fib();    // 1
  fib();    // 1
  fib();    // 2
  fib();    // 3
  fib();    // 5
*/
function* fibonaccif() {
    let fibA = 0;
    let fibB = 1;
    while (true) {
        let fibC = fibA + fibB;
        fibA = fibB;
        fibB = fibC;
        yield fibC;
    }
}

const fib = fibonaccif();
console.log('Task 13: ' + fib.next().value);
console.log('Task 13: ' + fib.next().value);
console.log('Task 13: ' + fib.next().value);
console.log('Task 13: ' + fib.next().value);
console.log('Task 13: ' + fib.next().value);

/*
    14. Create a generic iterator function each(array, fn), which can be used to iterate over arrays.

    const testArray = [0, 5, 2, 7, 12];

    each(testArray, (element) => console.log(element));
*/
const testArray = [0, 5, 2, 7, 12];
const fn = (element) => console.log(element);

function each (array, fn) {
    let i = 0;
    while (array[i] !== undefined){
        fn(array[i]);
        i++;
    }
}

each(testArray, (element) => console.log('Task 14: ' + element));

/*
  15.

  a) Make a function that makes a publish/subscribe object. It will reliably deliver all publications to all subscribers in the right order.

  const myPubsub = pubsub();
  myPubsub.subscribe(function(data){
      console.log('Subscriber one says: ' + data);
  });
  myPubsub.subscribe(function(message){
      console.log('Subscriber two says: ' + message);
  });
  myPubsub.subscribe(function(message){
      console.log('Subscriber three says: ' + message);
  });

  myPubsub.publish('hello!');

  // 'Subscriber one says: hello!'
  // 'Subscriber two says: hello!'
  // 'Subscriber three says: hello!'
*/
const subscribers = {
    subscribersArray: []
};
let initialArr = subscribers.subscribersArray;

function pubsub(arr) {

    const subscribe = func => arr.push(func);
    const publish = data => arr.forEach( fn => fn(data) );

    return {
        subscribe,
        publish
    }
}

console.log('Task 15 a): ');
const myPubsub = pubsub(initialArr);

myPubsub.subscribe(function(data){
    console.log('Subscriber one says: ' + data);
});
myPubsub.subscribe(function(data){
    console.log('Subscriber two says: ' + data);
});
myPubsub.subscribe(function(data){
    console.log('Subscriber three says: ' + data);
});

myPubsub.publish('hello!');

/*
  b) Make a function that use pubsub to create event manager. That attach an event handler function for one or more events.

        const manager = eventManager();

  manager.on('event1', function(data){
      console.log('event1_handler1 says: ' + data);
  });
  manager.on('event2', function(data){
        console.log('event2_handler1 says: ' + data);
  });
  manager.on('event2', function(data){
      console.log('event2_handler2 says: ' + data);
  });
  manager.on('event3', function(data){
        console.log('event3_handler1 says: ' + data);
  });
  manager.on('event2', function(data){
      console.log('event2_handler3 says: ' + data);
  });

  // execute all handlers only for event2
  manager.fire('event2', 'hello!');

  // 'event2_handler1 says: hello!'
  // 'event2_handler2 says: hello!'
  // 'event2_handler3 says: hello!'
*/
function eventManager(obj) {
    function on(event, func) {
        if (!Array.isArray(subscribers[event])) {
            subscribers[event] = []
        }
        pubsub(subscribers[event]).subscribe(func.bind(obj));
        return this;
    }

    function fire(event, data) {
        if (!Array.isArray(subscribers[event])) {
            return this;
        }
        pubsub(subscribers[event]).publish(data);
        return this;
    }

    function off(event) {
        delete subscribers[event];
        return this;
    }

    return {
        on,
        fire,
        off
    }
}

console.log('Task 15 b): ');
const manager = eventManager();
manager.on('event1', function(data){
    console.log('event1_handler1 says: ' + data);
});
manager.on('event2', function(data){
    console.log('event2_handler1 says: ' + data);
});
manager.on('event2', function(data){
    console.log('event2_handler2 says: ' + data);
});
manager.on('event3', function(data){
    console.log('event3_handler1 says: ' + data);
});
manager.on('event2', function(data){
    console.log('event2_handler3 says: ' + data);
});

manager.fire('event2', 'hello!');

/*
  c) Make possible to remove an event handler by name.

  manager.off('event2');
  manager.fire('event2', 'hello!');
  // no handlers are executed
*/
console.log('Task 15 c): ');
manager.off('event2');
manager.fire('event2', 'hello!');

/*
  d) Make possible to call fluently.

  const manager = eventManager();

  manager
        .on('event1', function(data){// code })
        .on('event2', function(data){// code })
        .on('event2', function(data){// code })
        .on('event3', function(data){// code })
        .on('event2', function(data){// code })
        .off('event1')
        .fire('event2', 'hello!');

  // 'event2_handler1 says: hello!'
  // 'event2_handler2 says: hello!'
  // 'event2_handler3 says: hello!'
*/
console.log('Task 15 d): ');
manager
    .on('event1', function(data){ console.log('event1_handler1 says: ' + data) })
    .on('event2', function(data){ console.log('event2_handler1 says: ' + data) })
    .on('event2', function(data){ console.log('event2_handler2 says: ' + data) })
    .on('event3', function(data){ console.log('event3_handler1 says: ' + data) })
    .on('event2', function(data){ console.log('event2_handler3 says: ' + data) })
    .off('event1')
    .fire('event2', 'hello!');

/*
  e) Make possible to provide context.

  const obj = { separator: '*' };
  const manager = eventManager(obj);

  manager
      .on('event1', function(data){
          var self = this;
          var message = ['event1_handler1','says', ':', data];

          console.log(message.join(self.separator));
          console.log(self === obj);
      })
      .fire('event1', 'hello!');

  // 'event1_handler1*says*:*hello!'
  // true
*/
console.log('Task 15 e): ');
const obj = { separator: '*' };
const manager2 = eventManager(obj);

manager2
    .on('event1', function(data){
        let self = this;
        let message = ['event1_handler1','says', ':', data];

        console.log(message.join(self.separator));
        console.log(self === obj);
    })
    .fire('event1', 'hello!');











